from unittest import TestCase
from copy import deepcopy

from deepdiff import DeepDiff

from adt import Stack


class ADTtestSuite(TestCase):

    def setUp(self):
        self.S = Stack()
        
    def test_create_stack(self):
        self.assertTrue(isinstance(self.S, Stack))
    
    def test_push_and_popoff_stack(self):
        T = deepcopy(self.S)
        T.push(1).
        T.pop()
        self.assertFalse(DeepDiff(T, self.S))

