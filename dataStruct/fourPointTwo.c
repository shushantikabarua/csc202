#include <stdio.h>
#include "mystdio.h"
#include <ctype.h>

#define MAXLINE 100

/* atof: convert string s to double */
double atof(char s[])
{
    double val, power;
    int expo, i, sign;
    double answer;

    for (i = 0; isspace(s[i]); i++)   /* skip white space */
        ;
    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '-')
        i++;
    for (val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');
    if (s[i] == '.')
        i++;
    for (power = 1.0; isdigit(s[i]); i++) {
        val = 10.0 * val + (s[i] - '0');
        power *= 10.0;
    answer = sign * val / power;
    if (s[i] == 'e' || s[i] == 'E'){
        sign = (s[i++] == '-') ? -1: 1;
        if (s[i] == '+' || s[i] == '-')
            i++;
    for (expo = 0; isdigit(s[i]); i++)
        expo = 10.0 * expo + (s[i] - '0');
    if (sign == 1)
        while (expo-- > 0)
            val *= 10;
    else 
        while (expo-- > 0)
            val /= 10;
    }
    }
    return answer;
}

/* rudimentary adding machine */
int main()
{
    double sum, atof(char []);
    char line[MAXLINE];

    sum = 0;
    while (mygetline(line, MAXLINE) > 0)
        printf("\t%g\n", sum += atof(line));

    return 0;
}

