from shushantools import Stack                      # import a stack

def parChecker(symbolString):
    s = Stack()
    balanced = True
    index = 0
    while index < len(symbolString) and balanced:
        symbol = symbolString[index]
        if symbol == "(":                           # if current symbol is a ( then push onto stack
            s.push(symbol)
        else:
            if s.isEmpty():
                balanced = False
            else:
                s.pop()                             # if current symbol is a ) then pop the ( that was there last

        index = index + 1

    if balanced and s.isEmpty():                    # if stack is empty and expression is balanced, then string is correctly balanced
        return True
    else:
        return False                                # if not balanced equation, then print false

print(parChecker('((()))'))
print(parChecker('(()'))

