from statemachine import StateMachine

# create state machine called m
def not_in_ould(text):
    current_input = text[0]
    if current_input == "\n":
        state = "no_match"
    if current_input == "o":
        state = "o"
    else:
        state = "not_in_ould"
    return (state, text[1:])

def o_state(text):
    current_input = text[0]
    if current_input == "\n":
        state = "no_match"
    if current_input == "u":
        state = "u"
    else:
        state = "not_in_ould"
    return (state, text[1:])

def u_state(text):
    current_input = text[0]
    if current_input == "\n":
        state = "no_match"
    if current_input == "l":
        state = "l"
    else:
        state = "not_in_ould"
    return (state, text[1:])

def l_state(text):
    current_input = text[0]
    if current_input == "\n":
        state = "no_match"
    if current_input == "d":
        state = "yes_match"
    else:
        state = "not_in_ould"
    return (state, text[1:])

m = StateMachine()

# add states
m.add_state("o", o_state)
m.add_state("u", u_state)
m.add_state("l", l_state)
m.add_state("not_in_ould", not_in_ould)

# add final states
m.add_state("yes_match", None, 1)
m.add_state("no_match", None, 1)

# set state to start in
m.set_start("not_in_ould")

# test
m.run("this will print ould\n")
m.run("this will ould print\n")
m.run("this will not prin ou l d\n")
m.run("this will not print\n")
