#include <stdio.h>

/* print histogram of frequencies of different characters in its input*/
int main()
{
    int charbank[127] = {};
    int c, i, p;

    while ((c = getchar()) != EOF) 
        charbank[c]++;

    for (i = 0; i < 127; i++)
    {
        if (charbank[i] != 0)
        {
            printf(" %d:\t", i);

            for (p = 0; p < charbank[i]; p++)
                putchar('X');

            putchar('\n');
        }
    }
    return 0;
}
