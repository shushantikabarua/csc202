#include <stdio.h>

int fah2Cel(int f)
{
    int cel;
    
    cel = 5 * (f - 32) / 9;
    return cel;
}

int main()
{
    int fah;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;
    
    fah = lower;
    while (fah <= upper)
    {
        printf("%d\t %d\n", fah, fah2Cel(fah));
        fah = fah + step;
    }
}
