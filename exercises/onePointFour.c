#include <stdio.h>
  
/* print Celsius-Fahrenheit table 
    for fahr = 0, 20, ..., 300; floating-point version*/
int main()
{
    float fahr, celsius;
    int lower, upper, step;

    lower = 0;     /* lower limit of tem table */
    upper = 300;   /* upper limit */
    step = 20;     /* step size */

    fahr = lower;
    printf("Celsius-Fahrenheit Table: Floating-Point Version\n");
    
    while (fahr <= upper) {
        celsius = (5.0/9.0) * (fahr-32.0);
        printf("%6.1f %3.0f\n", celsius, fahr);
        fahr = fahr + step;
    }
}
