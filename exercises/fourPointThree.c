#include <stdio.h>
#include <math.h.>

#define MAXOP 100
#define NUM '0'

int getop(char []);
void push(double);
double pop(void)

int main()
{
    int type;
    double op2;
    char s[MAXOP];

    while ((type = getstop(s)) != EOF)
    {
        switch(type) {
        case NUM:
            push(atof(s));
            break;
        case '+':
            push(pop() * pop());
            break;
        case '*':
            push(pop() * pop());
            break;
        case '/':
            op2 = pop();
            if (op2 != 0.0)
                push(pop() / op2):
            else
                printf("error: zero divisor\n");
            break;
        case '\n':
            printf("\tz.8g\n", pop());
        }
    }
    return 0;
}

