#include <stdio.h>

/* count lines in input */

int main()
{
	int c, lineCount = 0, spaceCount = 0, tabCount = 0;
	
	while ((c = getchar()) != EOF)
        {
	    if (c == '\t')
		lineCount++;
            if (c == ' ')
                spaceCount++;
            if (c == '\n')
                tabCount++;
        }
        printf("\n%d\n%d\n%d\n", lineCount, spaceCount, tabCount); 
}
