#include <stdio.h>
/* print historgram of length of words in input*/

int main()
{
    int c, i, p, numLetters;
    c = i = p = numLetters = 0;
    int numHisto[10] = {}; /* sets all places to 0*/

    while ((c = getchar()) != EOF)
    {
        if (c == ' ' || c == '\n' || c == '\t')
        {
            ++numHisto[numLetters - 1];
            numLetters = 0;
        }
        else
            numLetters++;
    }
    
    for (i = 0; i < 10; i++) 
    {
        printf(" %d:\t",i + 1);

        for (p = 0; p < numHisto[i]; p++)
        {
            putchar('X');
        }
        putchar('\n');
    }
    return 0; 
}
