#include <stdio.h>
#define MAXLINE 1000  /* maximum input line size */

int getline(char line[], int maxline);
void reverse(char s[]);

/* reverse input lines, a line at a time */
int main()
{
    char line[MAXLINE];  /* current input line */

    while (getline(line, MACLINE) > 0)
    {
        reverse(line);
        printf("%s", line);
    }
}

/* reverse: reverse string s */
void reverse(char s[])
{
    int i, j;
    char temp;

    i = 0;
    while(s[i] != '\0' /* find the end of string s */
            ++i;
    --i; /* back off from '\0' */
    while (s[i] == '\n') 
        --i; /* leave newline in place */
    j = 0; /* beginning of new string s */
    while (j < i)
    {
        temp = s[j];
        s[j] = s[i]; /* swap te characters */
        s[i] = temp;
        --i;
        ++j;
    }
}
